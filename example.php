<?php
include("db.php");
include("session.php");
include("function.php");

if(isset($sMode) && $sMode >= 1) {
    $startrows = 0;
    $rows = 30;
    $sort = "";
    $filter = [];

    if(isset($_POST['p']) && $_POST['p'] >= 1) {
        $startrows = $rows * $_POST['p'] - $rows;
    }

    foreach ($_POST as $query=>$list){
		switch($query) {
			case 'gender': 
				$list = implode("|", $list);
				$filter[] = "AND Gender RLIKE '".$list."'";
				break;
			case 'country':
				$list = implode("|", $list);
				$filter[] = "AND country.country_name RLIKE '".$list."'";
				break;
			case 'city': 
				$list = implode("|", $list);
				$filter[] = "AND city.city_name RLIKE '".$list."'";
				break;
			case 'mission': 
				$list = implode("|", $list);
				$filter[] = "AND Mission RLIKE '".$list."'";
				break;
		}
	}

    $minbirth = date('Y-m-d');
    $maxbirth = date('Y-m-d', strtotime("- 100 years"));
    if(!empty($_POST['minage'])) {
        $minbirth = getBirthday($_POST['minage']);
    }
    if(!empty($_POST['maxage'])) {
        $maxbirth = date('Y-m-d', strtotime(getBirthday($_POST['maxage'])."- 1 year"));
    }
    $filter[] = "AND (Birthday BETWEEN '$maxbirth' AND '$minbirth')";

    $filter = implode(" ", $filter);

    $result = $mysqli->query("SELECT * 
                                      FROM `users` 
                                      LEFT JOIN country ON users.Country = country.country_id 
                                      LEFT JOIN city ON users.City = city.city_id 
                                      WHERE DateUpForm > SUBDATE(CURDATE(), INTERVAL 90 DAY) AND SettingsShowForm = '1' AND Mode >= '1' AND Active != 'ban' $filter 
                                      ORDER BY DateUpForm DESC LIMIT $startrows, $rows");
    if($result->num_rows == 0) {
        echo "<div style='text-align: center; margin: 10px;'><h3>Nope. :(</h3></div>";
    } else {
        foreach ( $result as $row ) {
            $mission = [];
            $urlicon = "http://" .$_SERVER['SERVER_NAME']."/public/assets/images/icon/";
            if(preg_match("/(Wid)/", $row['Mission'])) {
                $icon = "";
                if($row['Gender'] == "Man") {
                    $icon = "iconMan.png";
                } else {
                    $icon = "iconWoman.png";
                }
                $mission[] = "<span style='background-image: url(". $urlicon.$icon . ")' title='wid'></span>";
            }
            $mission = "<div class='icon-mission'>".implode("", $mission)."</div>";
            ?>
            <div class="selected-user" user-id='<?=$row['ID']?>' style="<?php if($row['Gender'] == 'Woman'){ echo 'border-color: #f26fb0';}?>">
                <div class="selected-user-prev">
                    <div class="selected-user-prev-info">
                        <div class="avatar-form" style="background-image: url(<?=$row['Avatar']?>)"></div>
                        <span class="selected-user-name"><?=$row['Name']?></span>
                        <span class="selected-user-age">Age: <?=getAge($row['Birthday']);?></span>
                        <?=$mission?>
                        <div class="selected-user-location" style="<?php if($row['Gender'] == 'Woman'){ echo 'background-color: #f26fb0';}?>">
                            <div><?=$row['country_name']?></div>
                            <div><?=$row['city_name']?></div>
                        </div>
                    </div>
                    <?php
                    $prevabout = $row['AboutMe'];
                    if(strlen($prevabout) > 100)
                        $prevabout = mb_substr($prevabout, 0, 100)."...";
                    ?>
                    <div class="selected-user-prev-about"><?=$prevabout;?></div>
                </div>
                <div class="selected-user-info">
                    <h3><a href="/public/profile.php?profile=<?=$row['ID']?>">Profile</a></h3>
                    <?php
                    $contacts = array(
                        "1" => $row["Profile1"],
                        "2" => $row["Profile2"],
                        "3" => $row["Profile3"]
                    );
                    if($row['SettingsHideEmail'] == 0) {
                        $contacts=array("email" => $row['Email'])+$contacts;
                    }
                    getUserContacts($contacts);
                    
                    if(!empty($row['Favorite'])) { ?>
                        <div><h4>Favorite:</h4><span><?=nl2br($row['Favorite']);?></span></div>
                        <hr>
                        <?php
                    }
                    ?>
                    <div><h4>Find:</h4><span><?=nl2br($row['Find']);?></span></div>
                </div>
            </div>
        <?php
        }
        if($result->num_rows > 20 || $_POST['p'] > 1) {
            echo "<span class='up-forms'>&#8628;</span>";
            if($result->num_rows == 30) {
                echo "<span class='next-forms'>Next</span>";
            } else {
                echo "<span class='next-forms_disabled'>Nope :(</span>";
            }
        }
    }
    exit();
}
?>
